import os
from flask import Flask, jsonify, render_template, request
from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String, ForeignKey
from flask_sqlalchemy import SQLAlchemy



app = Flask(__name__)
# engine = create_engine('sqlite:///college.db', echo=True)
app.config['SECRET_KEY'] = "600373a16f6ca9ac4579a2075673cff3"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///day2.db"



db = SQLAlchemy(app)



class Employee(db.Model):

    emp_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(500))
    dep_id = db.Column(db.Integer, db.ForeignKey('department.dep_id'))
    manager = db.Column(db.Integer, db.ForeignKey('employee.emp_id'), nullable=True)

    def __repr__(self):
        print(self.name)
        print(self.dep_id)
        print(Department.query.get(self.dep_id).name)
        print(Employee.query.get(self.manager).name)
        print('---')
        return f"({self.emp_id}, {self.name}, {Department.query.get(self.dep_id).name}, {Employee.query.get(self.manager).name})"


class Department(db.Model):

    dep_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(500))
    emp = db.relationship('Employee', backref='department')

    def __repr__(self):
        return f"({self.dep_id}, {self.name})"








@app.before_first_request
def create_table():
    db.create_all()



#CRUD OPERATION
# Create 
    # Adding Department
@app.route('/add_dep', methods = ['POST'])
def createDep():

    name = request.form['name']
    obj = Department( name  = name )

    db.session.add(obj)
    db.session.commit()

    return f'successfully added {obj}'
    # Adding Employees
@app.route('/add_emp', methods = ['POST']) 
def createEmp():
    name = request.form['name']
    dep_id = request.form['dep_id']
    manager = request.form['manager']
    print(name, dep_id, manager)

    obj = Employee(name=name, dep_id=dep_id, manager=manager)    
    db.session.add(obj)
    db.session.commit()
    print(obj)
    return f'successfuly added {obj}'






# Read
@app.route("/")
def home():
    # obj = Employee.query.delete()
    # print(obj)
    emps = Employee.query.all()
    deps = Department.query.all()
    # db.session.commit()
    # return 'h/omePage'
    return jsonify({"Employees": emps.__repr__(), "Departments": deps.__repr__()})

    # Query One: Query for all employees info  and should contain their department and manager.
@app.route("/query1")
def query1():
    emps = Employee.query.all()

    return jsonify({"Employees": emps.__repr__()})

    # Query Two: Query for all departments should contain all employees present , name and their manager.
@app.route("/query2")
def query2():
    deps = Department.query.all()
    result = []
    for dep in deps:
        employees = Employee.query.filter_by(dep_id = dep.dep_id).all()
        result.append((dep.name, employees))

    return repr(result)







# UPDATE
    # update Emp
@app.route('/update_emp', methods=['PUT'])
def update_employee():
    id = request.form.get('id')
    emp = Employee.query.get(id)
    emp.name = request.form.get('name')
    emp.dep_id = request.form.get('dep_id')
    emp.manager = request.form.get('manager')
    db.session.commit()

    return 'Update Emp Success'

    # Update Dep
@app.route('/update_department', methods=['PUT'])
def update_department():
    id = request.form.get('id')
    dep = Department.query.get(id)
    dep.name = request.form.get('name')
    db.session.commit()

    return 'Update Dep Success'





# DELETE
    #Delete Employee
@app.route('/remove_emp', methods=['DELETE'])
def remove_employee():
    id = request.form.get('id')
    emp = Employee.query.get(id)
    db.session.delete(emp)
    db.session.commit()

    return 'Delete Emp Success'

    # Delete Department
@app.route('/remove_dep', methods=['DELETE'])
def remove_department():
    id = request.form.get('id')
    dep = Department.query.get(id)
    db.session.delete(dep)
    db.session.commit()

    return 'Delete Dep Success'




if __name__ == '__main__':
    app.run(debug = True)
    